function TemplateController(iwDataService, $interval) {
	this.pagination = {
		currentCategoryIndex: 0,
		itemsPerPage: 20,
		currentPage: 1,
		slideDuration: 2,
		currentTotalPages: 1
	}

	iwDataService.get().then(server => {
		/**
		 *
		 * The following code is temporary until filters are fully implemented.
		 * Without specifying any filters, Each page would render the entire array of sample data.
		 *
		 *
		 * Completed code should be `this.pages = window.IWUtils.applyEffects(server.data, server.settings)`
		 *
		 */

		//this.pages = window.IWUtils.applyEffects(server.data, server.settings)
		this.pages = window.IWUtils.applyEffects(server.data, {
			fieldMappings: server.settings.fieldMappings,
			page: server.settings.page.map((page, idx) => ({
				...page,
				filters: {
					gift_amount:
						idx % 2 === 0
							? {
									gte: "10000"
							  }
							: {
									lt: "10000"
							  }
				}
			}))
		})

		// Get the first "page's|category's" total amount of items.
		this.pagination.totalPages = Math.max(
			Math.ceil(this.pages[this.pagination.currentCategoryIndex].items.length / this.pagination.itemsPerPage),
			1
		)

		$interval(() => {
			this.pagination.currentPage++
			this.pagination.currentTotalPages++
			// when we reach the end of the items
			if (this.pagination.currentPage > this.pagination.totalPages) {
				// set the currentPage back to 1
				this.pagination.currentPage = 1

				// figure out the correct category index, are we incrementing or going back to the first iterable
				this.pagination.currentCategoryIndex =
					this.pagination.currentCategoryIndex + 1 >= this.pages.length ? 0 : this.pagination.currentCategoryIndex + 1

				if (this.pagination.currentCategoryIndex === 0) {
					this.pagination.currentTotalPages = 1
				}
				// figure out the new totalPages based on the new pagination.currentCategoryIndex
				this.pagination.totalPages = Math.max(
					Math.ceil(this.pages[this.pagination.currentCategoryIndex].items.length / this.pagination.itemsPerPage),
					1
				)
			}
		}, this.pagination.slideDuration * 1000)

		this.totalPages = this.pages.reduce((acc, { items = [] }) => {
			return acc + Math.max(Math.ceil(items.length / this.pagination.itemsPerPage), 1)
		}, 0)
	})
}

const app = angular.module("app")
app.controller("TemplateController", ["iwDataService", "$interval", TemplateController])
